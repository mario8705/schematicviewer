#version 330

layout (location = 0) in vec3 aPosition;
layout (location = 1) in vec2 aTexCoord;
layout (location = 2) in vec3 aNormal;

uniform mat4 uModelViewProjection;
uniform mat4 uModel;

varying vec3 vWorldPos;
varying vec2 vTexCoord;
varying vec3 vNormal;

void main() {
	gl_Position = uModelViewProjection * vec4(aPosition, 1.0);
	
	mat3 normalMatrix = transpose(inverse(mat3(uModel)));
	
	vWorldPos = (uModel * vec4(aPosition, 1.0)).xyz;
	vTexCoord = aTexCoord;
	vNormal = normalMatrix * aNormal;
}