package fr.mario8705.schematicviewer;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

public final class MeshData {
	public List<Vector3f> vertices;
	public List<Vector2f> texCoords;
	public List<Vector3f> normals;
	public List<Integer> triangles;
	
	public MeshData() {
		this.vertices = new ArrayList<>();
		this.texCoords = new ArrayList<>();
		this.normals = new ArrayList<>();
		this.triangles = new ArrayList<>();
	}
	
	public void addTriangle(int a, int b, int c) {
		triangles.add(a);
		triangles.add(b);
		triangles.add(c);
	}
	
	public void addQuadFace() {
		int vSize = vertices.size();
		
		addTriangle(
			vSize - 1,
			vSize - 2,
			vSize - 4
		);
		
		addTriangle(
			vSize - 2,
			vSize - 3,
			vSize - 4
		);
	}
	
	public void addQuadNormal(Vector3f normal) {
		for (int i = 0; i < 4; i++) {
			normals.add(normal);
		}
	}
}
