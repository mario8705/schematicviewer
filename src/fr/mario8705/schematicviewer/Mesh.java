package fr.mario8705.schematicviewer;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.util.List;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

public final class Mesh {
	private int vertexBuffer;
	private int texCoordBuffer;
	private int normalBuffer;
	private int indexBuffer;
	private int indicesCount, indicesType;
	private Texture texture;
	private Shader shader;
	
	public Mesh(MeshData meshData) {
		this(meshData, null);
	}
	
	public Mesh(MeshData meshData, Texture texture) {
		this.texture = texture;
		
//		recalculateNormals(meshData);
		
		FloatBuffer vertices = BufferUtils.createFloatBuffer(meshData.vertices.size() * 3);
		for (Vector3f vertex : meshData.vertices) {
			vertices.put(vertex.x);
			vertices.put(vertex.y);
			vertices.put(vertex.z);
		}
		vertices.flip();
		
		this.vertexBuffer = GL15.glGenBuffers();
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vertexBuffer);
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, vertices, GL15.GL_STATIC_DRAW);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
		
		FloatBuffer texCoords = BufferUtils.createFloatBuffer(meshData.texCoords.size() * 2);
		for (Vector2f texCoord : meshData.texCoords) {
			texCoords.put(texCoord.x);
			texCoords.put(texCoord.y);
		}
		texCoords.flip();
		
		this.texCoordBuffer = GL15.glGenBuffers();
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, texCoordBuffer);
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, texCoords, GL15.GL_STATIC_DRAW);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
		
		FloatBuffer normals = BufferUtils.createFloatBuffer(meshData.normals.size() * 3);
		for (Vector3f normal : meshData.normals) {
			normals.put(normal.x);
			normals.put(normal.y);
			normals.put(normal.z);
		}
		normals.flip();
		
		this.normalBuffer = GL15.glGenBuffers();
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, normalBuffer);
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, normals, GL15.GL_STATIC_DRAW);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
		
		this.indexBuffer = createOptimizedIndexBuffer(meshData.triangles);
	}
	
	public Texture getTexture() {
		return texture;
	}
	
	public void setTexture(Texture texture) {
		this.texture = texture;
	}
	
	public Shader getShader() {
		return shader;
	}
	
	public void setShader(Shader shader) {
		this.shader = shader;
	}
	
	public void recalculateNormals(MeshData meshData) {
		for (int i = 0; i < meshData.triangles.size(); i += 3) {
			Vector3f p1 = meshData.vertices.get(meshData.triangles.get(i));
			Vector3f p2 = meshData.vertices.get(meshData.triangles.get(i + 1));
			Vector3f p3 = meshData.vertices.get(meshData.triangles.get(i + 2));
			
			Vector3f u = new Vector3f();
			Vector3f v = new Vector3f();
			
			Vector3f.sub(p2, p1, u);
			Vector3f.sub(p3, p1, v);
			
			Vector3f normal = new Vector3f();
			normal.x = (u.y * v.z) - (u.z * v.y);
			normal.y = (u.z * v.x) - (u.x * v.z);
			normal.z = (u.x * v.y) - (u.y * v.x);
			
			meshData.normals.add(normal);
			meshData.normals.add(normal);
			meshData.normals.add(normal);
		}
	}
	
	private int createOptimizedIndexBuffer(List<Integer> indices) {
		int max = 0;
		
		for (int index : indices) {
			max = Math.max(index, max);
		}
		
		int indexBufferID = GL15.glGenBuffers();
		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, indexBufferID);
		
		if (max <= 0xff) {
			ByteBuffer buf = BufferUtils.createByteBuffer(indices.size() * Byte.BYTES);
			
			for (int index : indices) {
				buf.put((byte) (index & 0xff));
			}
			
			buf.flip();
			GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, buf, GL15.GL_STATIC_DRAW);
			
			this.indicesType = GL11.GL_UNSIGNED_BYTE;
		}
		else if (max <= 0xffff) {
			ShortBuffer buf = BufferUtils.createShortBuffer(indices.size() * Short.BYTES);
			
			for (int index : indices) {
				buf.put((short) (index & 0xffff));
			}
			
			buf.flip();
			GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, buf, GL15.GL_STATIC_DRAW);
			
			this.indicesType = GL11.GL_UNSIGNED_SHORT;
		}
		else {
			IntBuffer buf = BufferUtils.createIntBuffer(indices.size() * Integer.BYTES);
			
			for (int index : indices) {
				buf.put(index);
			}
			
			buf.flip();
			GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, buf, GL15.GL_STATIC_DRAW);
			
			this.indicesType = GL11.GL_UNSIGNED_INT;
		}
		
		this.indicesCount = indices.size();
		
		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
		return indexBufferID;
	}
	
	public void render(Camera camera) {
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glDepthFunc(GL11.GL_LEQUAL);
		
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glCullFace(GL11.GL_BACK);
		GL11.glFrontFace(GL11.GL_CCW);
		
//		GL11.glEnable(GL11.GL_ALPHA_TEST);
//		GL11.glAlphaFunc(GL11.GL_GREATER, 0.0f);
		
		Matrix4f projection = camera.getProjection();
		Matrix4f view = camera.getTransformation();
		Matrix4f model = new Matrix4f();
		Matrix4f mvp = new Matrix4f();
		
		Matrix4f.mul(projection, view, mvp);
		Matrix4f.mul(mvp, model, mvp);
		
		GL20.glUseProgram(shader.getProgramHandle());
		shader.setMatrix("uModelViewProjection", mvp);
		shader.setMatrix("uModel", model);
		
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, texture.getTextureID());
		
		GL20.glEnableVertexAttribArray(0);
		GL20.glEnableVertexAttribArray(1);
		GL20.glEnableVertexAttribArray(2);
		
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vertexBuffer);
		GL20.glVertexAttribPointer(0, 3, GL11.GL_FLOAT, false, 0, 0L);
		
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, texCoordBuffer);
		GL20.glVertexAttribPointer(1, 2, GL11.GL_FLOAT, false, 0, 0L);
		
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, normalBuffer);
		GL20.glVertexAttribPointer(2, 3, GL11.GL_FLOAT, false, 0, 0L);
		
		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
		GL11.glDrawElements(GL11.GL_TRIANGLES, indicesCount, indicesType, 0L);
		
		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
		
		GL20.glDisableVertexAttribArray(2);
		GL20.glDisableVertexAttribArray(1);
		GL20.glDisableVertexAttribArray(0);
		
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
		GL20.glUseProgram(0);
		
		GL11.glDisable(GL11.GL_ALPHA_TEST);
		GL11.glDisable(GL11.GL_CULL_FACE);
		GL11.glDisable(GL11.GL_DEPTH_TEST);
	}
	
	public static Mesh createMeshFromSchematic(Schematic schematic) {
		int width = schematic.getWidth(), height = schematic.getHeight(), length = schematic.getLength();
		BlockState[][][] blockStates = schematic.getBlockStates();
		
		MeshData meshData = new MeshData();
		
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				for (int z = 0; z < length; z++) {
					BlockRenderer blockRenderer;
					BlockState blockState;
					
					if ((blockState = blockStates[x][y][z]) != null && (blockRenderer = blockState.getBlock().getBlockRenderer()) != null) {
						blockRenderer.renderBlock(meshData, schematic, blockState, x, y, z);
					}
				}
			}
		}
		
		return new Mesh(meshData);
	}
}
