package fr.mario8705.schematicviewer;

public final class Schematic {
	private final int width, height, length;
	private final BlockState[][][] blockStates;
	
	public Schematic(int width, int height, int length, BlockState[][][] blockStates) {
		this.width = width;
		this.height = height;
		this.length = length;
		this.blockStates = blockStates;
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public int getLength() {
		return length;
	}
	
	public BlockState[][][] getBlockStates() {
		return blockStates;
	}
	
	public BlockState getBlockAt(int x, int y, int z) {
		if (x >= 0 && y >= 0 && z >= 0) {
			if (x < width && y < height && z < length) {
				return blockStates[x][y][z];
			}
		}
		
		return new BlockState(Block.air);
	}
}
