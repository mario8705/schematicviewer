package fr.mario8705.schematicviewer;

public final class BlockLeaves extends Block {
	public BlockLeaves(int id) {
		super(id);
	}
	
	@Override
	public boolean isSolid(BlockFace blockFace) {
		return false;
	}
	
	@Override
	public int getTextureFromFace(BlockFace blockFace) {
		return 11;
	}
}
