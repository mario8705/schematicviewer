package fr.mario8705.schematicviewer;

public final class BlockLog extends Block {
	public BlockLog(int id) {
		super(id);
	}
	
	@Override
	public int getTextureFromFace(BlockFace blockFace) {
		switch (blockFace) {
		case Top:
		case Bottom:
			return 10;
			
		default:
			return 9;
		}
	}
}
