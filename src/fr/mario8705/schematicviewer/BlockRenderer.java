package fr.mario8705.schematicviewer;

public abstract class BlockRenderer {
//	public abstract void renderBlock(Block block, int x, int y, int z, byte data);
	public abstract void renderBlock(MeshData meshData, Schematic schematic, BlockState blockState, int x, int y, int z);
}
