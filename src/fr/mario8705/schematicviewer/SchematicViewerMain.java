package fr.mario8705.schematicviewer;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.*;

import org.lwjgl.util.vector.Vector2f;

public final class SchematicViewerMain {
	private static SchematicViewerMain instance;
	private volatile boolean isRunning;
	private Camera camera;
	private Schematic schematic;
	private Mesh mesh;
	private Texture texture;
	private GBuffer gBuffer;
	private Shader deferredFinalShader;

	private SchematicViewerMain() {
		try {
			this.schematic = SchematicLoader.loadSchematic(new File("jardinsuspendu.schematic"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void run() {
		this.isRunning = true;

		try {
			Display.setTitle("Schematic Viewer");
			Display.setDisplayMode(new DisplayMode(800, 600));
			Display.setResizable(true);
			Display.setVSyncEnabled(true);
			Display.create();
		}
		catch (LWJGLException e) {
			e.printStackTrace();
		}

		System.out.println("OpenGL version: " + GL11.glGetString(GL11.GL_VERSION));
		System.out.println("OpenGL vendor: " + GL11.glGetString(GL11.GL_VENDOR));
		System.out.println("OpenGL renderer: " + GL11.glGetString(GL11.GL_RENDERER));

		try {
			this.texture = Texture.loadTexture(new File("terrain.png"));
		}
		catch (IOException e) {
			e.printStackTrace();
		}

		this.camera = new Camera();
		this.mesh = Mesh.createMeshFromSchematic(schematic);
		mesh.setTexture(texture);
		try {
			mesh.setShader(Shader.loadShaderPair(new File("deferred_vs.glsl"), new File("deferred_fs.glsl")));
			deferredFinalShader = Shader.loadShaderPair(new File("deferred_final_vs.glsl"), new File("deferred_final_fs.glsl"));
		}
		catch (IOException e) {
			e.printStackTrace();
		}

		this.gBuffer = new GBuffer();
		gBuffer.init(800, 600);

		float lastFrameTime = getTime();

		FloatBuffer screenQuadBuf = BufferUtils.createFloatBuffer(4 * 2);
		screenQuadBuf.put(-1.0f).put(1.0f);
		screenQuadBuf.put(1.0f).put(1.0f);
		screenQuadBuf.put(1.0f).put(-1.0f);
		screenQuadBuf.put(-1.0f).put(-1.0f);
		screenQuadBuf.flip();

		ByteBuffer screenQuadIndicesBuf = BufferUtils.createByteBuffer(6);
		screenQuadIndicesBuf.put((byte) 0).put((byte) 1).put((byte) 3);
		screenQuadIndicesBuf.put((byte) 1).put((byte) 2).put((byte) 3);
		screenQuadIndicesBuf.flip();

		while (isRunning) {
			float time = getTime();

			float deltaTime = time - lastFrameTime;
			lastFrameTime = time;

			gBuffer.bindForWriting();

			GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
			GL11.glClearDepth(1.0f);
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);

			GL11.glViewport(0, 0, Display.getWidth(), Display.getHeight());

			camera.update(deltaTime);
			mesh.render(camera);

			GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, 0);

			GL11.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
			GL11.glClearDepth(1.0f);
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);

			GL20.glUseProgram(deferredFinalShader.getProgramHandle());
			deferredFinalShader.setVector2("uScreenSize", new Vector2f(Display.getWidth(), Display.getHeight()));
			deferredFinalShader.setInt("uPosition", 0);
			deferredFinalShader.setInt("uAlbedo", 1);
			deferredFinalShader.setInt("uNormal", 2);
			deferredFinalShader.setInt("uTexCoord", 3);
			gBuffer.bindForReading();

			GL20.glEnableVertexAttribArray(0);
			GL20.glVertexAttribPointer(0, 2, false, 0, screenQuadBuf);
			GL11.glDrawElements(GL11.GL_TRIANGLES, screenQuadIndicesBuf);
			GL20.glDisableVertexAttribArray(0);

			GL20.glUseProgram(0);

			if (Mouse.isButtonDown(0)) {
				Mouse.setGrabbed(true);
			}

			while (Keyboard.next()) {
				boolean eventKeyState = Keyboard.getEventKeyState();
				int eventKey = Keyboard.getEventKey();

				if (eventKey == Keyboard.KEY_ESCAPE && !eventKeyState) {
					if (Mouse.isGrabbed()) {
						Mouse.setGrabbed(false);
					}
					else {
						isRunning = false;
					}
				}
			}

			if (Display.isCloseRequested())
				isRunning = false;

			Display.update();
			Display.sync(60);
		}

		Display.destroy();
	}

	public float getTime() {
		return (float) (((double) System.nanoTime()) / 1000000000.0D);
	}

	public static void main(String[] args) {
		instance = new SchematicViewerMain();
		instance.run();
	}
}
