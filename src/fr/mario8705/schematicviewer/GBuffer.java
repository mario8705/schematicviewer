package fr.mario8705.schematicviewer;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

public final class GBuffer {
	private int fbo;
	private int[] textures;
	private int depthTexture;
	
	public GBuffer() {
		this.textures = new int[TextureType.values().length];
	}
	
	public void init(int width, int height) {
		this.fbo = GL30.glGenFramebuffers();
		GL30.glBindFramebuffer(GL30.GL_DRAW_FRAMEBUFFER, fbo);
		
		for (int i = 0; i < textures.length; i++) {
			textures[i] = GL11.glGenTextures();
		}
		this.depthTexture = GL11.glGenTextures();
		
		for (int i = 0; i < textures.length; i++) {
			GL11.glBindTexture(GL11.GL_TEXTURE_2D, textures[i]);
			GL11.glTexParameterf(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
			GL11.glTexParameterf(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
			GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL30.GL_RGBA32F, width, height, 0, GL11.GL_RGBA, GL11.GL_FLOAT, (FloatBuffer) null);
			GL30.glFramebufferTexture2D(GL30.GL_FRAMEBUFFER, GL30.GL_COLOR_ATTACHMENT0 + i, GL11.GL_TEXTURE_2D, textures[i], 0);
		}
		
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, depthTexture);
        GL11.glTexParameterf(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
        GL11.glTexParameterf(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
        GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL30.GL_DEPTH_COMPONENT32F, width, height, 0, GL11.GL_DEPTH_COMPONENT, GL11.GL_FLOAT, (FloatBuffer) null);
        GL30.glFramebufferTexture2D(GL30.GL_FRAMEBUFFER, GL30.GL_DEPTH_ATTACHMENT, GL11.GL_TEXTURE_2D, depthTexture, 0);
	
        IntBuffer buf = BufferUtils.createIntBuffer(4);
        buf.put(GL30.GL_COLOR_ATTACHMENT0);
        buf.put(GL30.GL_COLOR_ATTACHMENT1);
        buf.put(GL30.GL_COLOR_ATTACHMENT2);
        buf.put(GL30.GL_COLOR_ATTACHMENT3);
        buf.flip();
        
        GL20.glDrawBuffers(buf);
        
        int status = GL30.glCheckFramebufferStatus(GL30.GL_FRAMEBUFFER);

        GL30.glBindFramebuffer(GL30.GL_DRAW_FRAMEBUFFER, 0);
	}
	
	public void bindForWriting() {
		GL30.glBindFramebuffer(GL30.GL_DRAW_FRAMEBUFFER, fbo);
	}

	public void bindForReading() {
		for (int i = 0; i < textures.length; i++) {
			GL13.glActiveTexture(GL13.GL_TEXTURE0 + i);
			GL11.glBindTexture(GL11.GL_TEXTURE_2D, textures[i]);
		}
	}

	public void setReadBuffer(TextureType textureType) {
		GL11.glReadBuffer(GL30.GL_COLOR_ATTACHMENT0 + textureType.ordinal());
	}

	public int getFramebufferID(TextureType textureType) {
		return textures[textureType.ordinal()];
	}
	
	public enum TextureType {
		Position,
		Diffuse,
		Normal,
		TexCoord,
	}
}
