package fr.mario8705.schematicviewer;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

public final class DefaultBlockRenderer extends BlockRenderer {
	@Override
	public void renderBlock(MeshData meshData, Schematic schematic, BlockState blockState, int x, int y, int z) {
		if (!schematic.getBlockAt(x, y, z + 1).getBlock().isSolid(BlockFace.Back)) {
			meshData.vertices.add(new Vector3f(x - 0.5f, y + 0.5f, z + 0.5f));
			meshData.vertices.add(new Vector3f(x + 0.5f, y + 0.5f, z + 0.5f));
			meshData.vertices.add(new Vector3f(x + 0.5f, y - 0.5f, z + 0.5f));
			meshData.vertices.add(new Vector3f(x - 0.5f, y - 0.5f, z + 0.5f));
			meshData.addQuadFace();
			addFaceUvs(meshData, blockState.getBlock().getTextureFromFace(BlockFace.Front));
			meshData.addQuadNormal(new Vector3f(0.0f, 0.0f, -1.0f));
		}

		if (!schematic.getBlockAt(x, y, z - 1).getBlock().isSolid(BlockFace.Front)) {
			meshData.vertices.add(new Vector3f(x + 0.5f, y + 0.5f, z - 0.5f));
			meshData.vertices.add(new Vector3f(x - 0.5f, y + 0.5f, z - 0.5f));
			meshData.vertices.add(new Vector3f(x - 0.5f, y - 0.5f, z - 0.5f));
			meshData.vertices.add(new Vector3f(x + 0.5f, y - 0.5f, z - 0.5f));
			meshData.addQuadFace();
			addFaceUvs(meshData, blockState.getBlock().getTextureFromFace(BlockFace.Back));
			meshData.addQuadNormal(new Vector3f(0.0f, 0.0f, 1.0f));
		}

		if (!schematic.getBlockAt(x - 1, y, z).getBlock().isSolid(BlockFace.Right)) {
			meshData.vertices.add(new Vector3f(x - 0.5f, y + 0.5f, z - 0.5f));
			meshData.vertices.add(new Vector3f(x - 0.5f, y + 0.5f, z + 0.5f));
			meshData.vertices.add(new Vector3f(x - 0.5f, y - 0.5f, z + 0.5f));
			meshData.vertices.add(new Vector3f(x - 0.5f, y - 0.5f, z - 0.5f));
			meshData.addQuadFace();
			addFaceUvs(meshData, blockState.getBlock().getTextureFromFace(BlockFace.Left));
			meshData.addQuadNormal(new Vector3f(1.0f, 0.0f, 0.0f));
		}

		if (!schematic.getBlockAt(x + 1, y, z).getBlock().isSolid(BlockFace.Left)) {
			meshData.vertices.add(new Vector3f(x + 0.5f, y + 0.5f, z + 0.5f));
			meshData.vertices.add(new Vector3f(x + 0.5f, y + 0.5f, z - 0.5f));
			meshData.vertices.add(new Vector3f(x + 0.5f, y - 0.5f, z - 0.5f));
			meshData.vertices.add(new Vector3f(x + 0.5f, y - 0.5f, z + 0.5f));
			meshData.addQuadFace();
			addFaceUvs(meshData, blockState.getBlock().getTextureFromFace(BlockFace.Right));
			meshData.addQuadNormal(new Vector3f(-1.0f, 0.0f, 0.0f));
		}

		if (!schematic.getBlockAt(x, y + 1, z).getBlock().isSolid(BlockFace.Bottom)) {
			meshData.vertices.add(new Vector3f(x - 0.5f, y + 0.5f, z - 0.5f));
			meshData.vertices.add(new Vector3f(x + 0.5f, y + 0.5f, z - 0.5f));
			meshData.vertices.add(new Vector3f(x + 0.5f, y + 0.5f, z + 0.5f));
			meshData.vertices.add(new Vector3f(x - 0.5f, y + 0.5f, z + 0.5f));
			meshData.addQuadFace();
			addFaceUvs(meshData, blockState.getBlock().getTextureFromFace(BlockFace.Top));
			meshData.addQuadNormal(new Vector3f(0.0f, 1.0f, 0.0f));

		}

		if (!schematic.getBlockAt(x, y - 1, z).getBlock().isSolid(BlockFace.Top)) {
			meshData.vertices.add(new Vector3f(x + 0.5f, y - 0.5f, z - 0.5f));
			meshData.vertices.add(new Vector3f(x - 0.5f, y - 0.5f, z - 0.5f));
			meshData.vertices.add(new Vector3f(x - 0.5f, y - 0.5f, z + 0.5f));
			meshData.vertices.add(new Vector3f(x + 0.5f, y - 0.5f, z + 0.5f));
			meshData.addQuadFace();
			addFaceUvs(meshData, blockState.getBlock().getTextureFromFace(BlockFace.Bottom));
			meshData.addQuadNormal(new Vector3f(0.0f, -1.0f, 0.0f));
		}
	}
	
	private void addFaceUvs(MeshData meshData, int i) {
		float tileSize = 1.0f / 32.0f;
		
		float tileX = i % 32;
		float tileY = i / 32;
		
		float baseX = tileX * tileSize;
		float baseY = tileY * tileSize;
		
		meshData.texCoords.add(new Vector2f(baseX + tileSize, baseY));
		meshData.texCoords.add(new Vector2f(baseX, baseY));
		meshData.texCoords.add(new Vector2f(baseX, baseY + tileSize));
		meshData.texCoords.add(new Vector2f(baseX + tileSize, baseY + tileSize));
	}
}
