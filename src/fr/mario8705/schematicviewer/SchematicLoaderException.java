package fr.mario8705.schematicviewer;

import java.io.IOException;

public class SchematicLoaderException extends IOException {
	private static final long serialVersionUID = 1L;
	
	public SchematicLoaderException() {

	}
	
	public SchematicLoaderException(String arg0) {
		super(arg0);
	}
	
	public SchematicLoaderException(Throwable arg0) {
		super(arg0);
	}
	
	public SchematicLoaderException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}
}
