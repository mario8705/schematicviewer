package fr.mario8705.schematicviewer;

import fr.mario8705.schematicviewer.blocks.BlockAir;
import fr.mario8705.schematicviewer.blocks.BlockGrass;
import fr.mario8705.schematicviewer.blocks.BlockSandstone;

public class Block {
	private static final Block[] blocks = new Block[256];
	private static final BlockRenderer defaultBlockRenderer = new DefaultBlockRenderer();
	protected int id;
	protected int tileIndex;
	
	protected Block(int id) {
		this(id, 0);
	}
	
	protected Block(int id, int tileIndex) {
		this.id = id;
		this.tileIndex = tileIndex;
		
		if (blocks[id] != null)
			throw new IllegalStateException();
		
		blocks[id] = this;	
	}
	
	public BlockRenderer getBlockRenderer() {
		return defaultBlockRenderer;
	}
	
	public boolean isSolid(BlockFace blockFace) {
		return true;
	}
	
	public int getTextureFromFace(BlockFace blockFace) {
		return tileIndex;
	}
	
	public final int getID() {
		return id;
	}
	
	public static Block byID(int id) {
		Block block;
		
		if (id >= 0 && id < 256 && (block = blocks[id]) != null) {
			return block;
		}
		
		return air;
	}
	
	public static final Block air = new BlockAir(0);
	public static final Block stone = new Block(1, 1);
	public static final Block grass = new BlockGrass(2);
	public static final Block dirt = new Block(3, 2);
	public static final Block cobblestone = new Block(4);
	public static final Block planks = new Block(5);
	public static final Block bedrock = new Block(7);
	public static final Block sand = new Block(12, 5);
	public static final Block goldOre = new Block(14, 12);
	public static final Block ironOre = new Block(15, 13);
	public static final Block coalOre = new Block(16, 14);
	public static final Block log = new BlockLog(17);
	public static final Block leaves = new BlockLeaves(18);
	public static final Block sandstone = new BlockSandstone(24);
	public static final Block glowstone = new Block(89, 8);
}
