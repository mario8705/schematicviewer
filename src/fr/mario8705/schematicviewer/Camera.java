package fr.mario8705.schematicviewer;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

public final class Camera {
	public Vector3f position;
	public Vector3f rotation;

	public Camera() {
		this.position = new Vector3f();
		this.rotation = new Vector3f();
	}

	public Matrix4f getProjection() {
		Matrix4f projectionMatrix = new Matrix4f();

		float fieldOfView = 60f;
		float aspectRatio = (float) Display.getWidth() / (float) Display.getHeight();
		float near_plane = 0.1f;
		float far_plane = 1000f;

		float y_scale = (float) (1.0f / Math.tan(Math.toRadians(fieldOfView / 2.0f)));
		float x_scale = y_scale / aspectRatio;
		float frustum_length = far_plane - near_plane;

		projectionMatrix.m00 = x_scale;
		projectionMatrix.m11 = y_scale;
		projectionMatrix.m22 = -((far_plane + near_plane) / frustum_length);
		projectionMatrix.m23 = -1;
		projectionMatrix.m32 = -((2 * near_plane * far_plane) / frustum_length);
		projectionMatrix.m33 = 0;
		
		return projectionMatrix;
	}

	public void applyProjection() {
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();

		GLU.gluPerspective(60.0f, ((float) Display.getWidth()) / ((float) Display.getHeight()), 0.01f, 1000.0f);
	}
	
	public Matrix4f getTransformation() {
		Matrix4f transformation = new Matrix4f();
		
		transformation.rotate((float) Math.toRadians(rotation.x), new Vector3f(1.0f, 0.0f, 0.0f), transformation);
		transformation.rotate((float) Math.toRadians(rotation.y), new Vector3f(0.0f, 1.0f, 0.0f), transformation);
		transformation.translate(position, transformation);
		
		return transformation;
	}

	public void applyTransformation() {
		GL11.glMatrixMode(GL11.GL_MODELVIEW);

		GL11.glRotatef(rotation.x, 1.0f, 0.0f, 0.0f);
		GL11.glRotatef(rotation.y, 0.0f, 1.0f, 0.0f);
		GL11.glTranslatef(position.x, position.y, position.z);
	}

	public void update(float deltaTime) {
		final float walkSpeed = 10.0f;
		final float mouseSpeed = 10.0f;

		if (Mouse.isGrabbed()) {
			if (Keyboard.isKeyDown(Keyboard.KEY_Z)) {
				position.x -= Math.sin(Math.toRadians(rotation.y)) * walkSpeed * deltaTime;
				position.z += Math.cos(Math.toRadians(rotation.y)) * walkSpeed * deltaTime;
			}

			if (Keyboard.isKeyDown(Keyboard.KEY_S)) {
				position.x -= Math.sin(Math.toRadians(rotation.y + 180.0f)) * walkSpeed * deltaTime;
				position.z += Math.cos(Math.toRadians(rotation.y + 180.0f)) * walkSpeed * deltaTime;
			}

			if (Keyboard.isKeyDown(Keyboard.KEY_Q)) {
				position.x -= Math.sin(Math.toRadians(rotation.y + 270.0f)) * walkSpeed * deltaTime;
				position.z += Math.cos(Math.toRadians(rotation.y + 270.0f)) * walkSpeed * deltaTime;
			}

			if (Keyboard.isKeyDown(Keyboard.KEY_D)) {
				position.x -= Math.sin(Math.toRadians(rotation.y + 90.0f)) * walkSpeed * deltaTime;
				position.z += Math.cos(Math.toRadians(rotation.y + 90.0f)) * walkSpeed * deltaTime;
			}

			if (Keyboard.isKeyDown(Keyboard.KEY_LSHIFT))
				position.y += deltaTime * walkSpeed;

			if (Keyboard.isKeyDown(Keyboard.KEY_SPACE))
				position.y -= deltaTime * walkSpeed;

			rotation.y += Mouse.getDX() * mouseSpeed * deltaTime;
			rotation.x -= Mouse.getDY() * mouseSpeed * deltaTime;
		}
	}
}
