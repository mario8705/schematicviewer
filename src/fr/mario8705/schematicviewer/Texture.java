package fr.mario8705.schematicviewer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;

import de.matthiasmann.twl.utils.PNGDecoder;

public final class Texture {
	private final int textureID;
	
	private Texture(int textureID) {
		this.textureID = textureID;
	}
	
	public void destroy() {
		if (GL11.glIsTexture(textureID)) {
			GL11.glDeleteTextures(textureID);
		}
	}
	
	public int getTextureID() {
		return textureID;
	}
	
	@Override
	protected void finalize() throws Throwable {
		destroy();
	}
	
	public static Texture loadTexture(File file) throws IOException {
		try (InputStream in = new FileInputStream(file)) {
			return loadTexture(in);
		}
	}
	
	public static Texture loadTexture(InputStream in) throws IOException {
		PNGDecoder decoder = new PNGDecoder(in);
		
		ByteBuffer buf = BufferUtils.createByteBuffer(decoder.getWidth() * decoder.getHeight() * 4);
		decoder.decode(buf, decoder.getWidth() * 4, PNGDecoder.Format.RGBA);
		buf.flip();
		
		int textureID = GL11.glGenTextures();
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureID);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
		GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA, decoder.getWidth(), decoder.getHeight(), 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, buf);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
		
		return new Texture(textureID);
	}
}
