package fr.mario8705.schematicviewer;

public final class BlockState {
	private final Block block;
	private byte metadata;
	
	public BlockState(Block block) {
		this(block, (byte) 0);
	}
	
	public BlockState(Block block, byte metadata) {
		this.block = block;
		this.metadata = metadata;
	}
	
	public Block getBlock() {
		return block;
	}
	
	public byte getMetadata() {
		return metadata;
	}
	
	public void setMetadata(byte metadata) {
		this.metadata = metadata;
	}
}
