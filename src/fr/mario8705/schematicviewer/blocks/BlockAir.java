package fr.mario8705.schematicviewer.blocks;

import fr.mario8705.schematicviewer.Block;
import fr.mario8705.schematicviewer.BlockFace;
import fr.mario8705.schematicviewer.BlockRenderer;

public final class BlockAir extends Block {
	public BlockAir(int id) {
		super(id);
	}
	
	@Override
	public BlockRenderer getBlockRenderer() {
		return null;
	}
	
	@Override
	public boolean isSolid(BlockFace blockFace) {
		return false;
	}
}
