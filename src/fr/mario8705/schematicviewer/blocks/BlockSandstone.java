package fr.mario8705.schematicviewer.blocks;

import fr.mario8705.schematicviewer.Block;
import fr.mario8705.schematicviewer.BlockFace;

public final class BlockSandstone extends Block {
	public BlockSandstone(int id) {
		super(id);
	}
	
	@Override
	public int getTextureFromFace(BlockFace blockFace) {
		switch (blockFace) {
		case Top:
			return 5;
			
		case Bottom:
			return 6;
			
		default:
			return 7;
		}
	}
}
