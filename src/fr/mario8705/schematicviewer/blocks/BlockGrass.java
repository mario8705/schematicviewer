package fr.mario8705.schematicviewer.blocks;

import fr.mario8705.schematicviewer.Block;
import fr.mario8705.schematicviewer.BlockFace;

public final class BlockGrass extends Block {
	public BlockGrass(int id) {
		super(id);
	}
	
	@Override
	public int getTextureFromFace(BlockFace blockFace) {
		switch (blockFace) {
		case Top:
			return 3;
		
		case Bottom:
			return 2;
		
		default:
			return 4;
		}
	}
}
