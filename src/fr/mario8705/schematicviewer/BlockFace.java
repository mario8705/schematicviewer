package fr.mario8705.schematicviewer;

public enum BlockFace {
	Front,
	Back,
	Top,
	Bottom,
	Left,
	Right,
}
