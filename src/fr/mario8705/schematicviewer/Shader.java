package fr.mario8705.schematicviewer;

import java.io.File;
import java.io.IOException;
import java.nio.FloatBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;

public final class Shader {
    private final int program;
    private Map<String, Integer> uniformCache;
    private FloatBuffer matrixCache;

    private Shader(int program) {
        this.program = program;
        this.uniformCache = new HashMap<>();
        this.matrixCache = BufferUtils.createFloatBuffer(16);
    }

    public void setMatrix(String uniform, Matrix4f matrix) {
    	int uniformLocation = getUniformLocation(uniform);
    	
    	matrix.store(matrixCache);
    	matrixCache.flip();
    	
    	GL20.glUniformMatrix4(uniformLocation, false, matrixCache);
    }

    public void setVector2(String uniform, Vector2f v) {
        int uniformLocation = getUniformLocation(uniform);

        GL20.glUniform2f(uniformLocation, v.x, v.y);
    }

    public void setInt(String uniform, int i) {
        int uniformLocation = getUniformLocation(uniform);

        GL20.glUniform1i(uniformLocation, i);
    }
    
    private int getUniformLocation(String uniform) {
    	if (uniformCache.containsKey(uniform)) {
    		return uniformCache.get(uniform);
    	}
    	
    	int uniformLocation = GL20.glGetUniformLocation(program, uniform);
    	uniformCache.put(uniform, uniformLocation);
    	
    	return uniformLocation;
    }

    public void destroy() {
        if (GL20.glIsProgram(program)) {
            GL20.glDeleteProgram(program);
        }
    }
    
    public int getProgramHandle() {
    	return program;
    }

    @Override
    protected void finalize() throws Throwable {
        destroy();
    }
    
    private static int loadShader(String shaderSource, int shaderType) {
        int shader = GL20.glCreateShader(shaderType);
        GL20.glShaderSource(shader, shaderSource);
        GL20.glCompileShader(shader);

        if (GL20.glGetShaderi(shader, GL20.GL_COMPILE_STATUS) != GL11.GL_TRUE) {
            String infoLog = GL20.glGetShaderInfoLog(shader, GL20.glGetShaderi(shader, GL20.GL_INFO_LOG_LENGTH));
            System.err.println("Shader compilation failed: " + infoLog);

            GL20.glDeleteShader(shader);

            return 0;
        }

        return shader;
    }

    public static Shader loadShaderPair(File vertexShaderPath, File fragmentShaderPath) throws IOException {
    	byte[] vertexShader = Files.readAllBytes(Paths.get(vertexShaderPath.toURI()));
    	byte[] fragmentShader = Files.readAllBytes(Paths.get(fragmentShaderPath.toURI()));

    	String vertexShaderStr = new String(vertexShader, StandardCharsets.UTF_8);
    	String fragmentShaderStr = new String(fragmentShader, StandardCharsets.UTF_8);
    	
    	return loadShaderPair(vertexShaderStr, fragmentShaderStr);
    }
    
    public static Shader loadShaderPair(String vertexShaderSource, String fragmentShaderSource) {
        int vertexShader = loadShader(vertexShaderSource, GL20.GL_VERTEX_SHADER);
        int fragmentShader = loadShader(fragmentShaderSource, GL20.GL_FRAGMENT_SHADER);

        if (vertexShader == 0 || fragmentShader == 0) {
            if (GL20.glIsShader(vertexShader)) {
                GL20.glDeleteShader(vertexShader);
            }

            if (GL20.glIsShader(fragmentShader)) {
                GL20.glDeleteShader(fragmentShader);
            }
        }

        int program = GL20.glCreateProgram();
        GL20.glAttachShader(program, vertexShader);
        GL20.glAttachShader(program, fragmentShader);
        GL20.glLinkProgram(program);

        GL20.glDetachShader(program, vertexShader);
        GL20.glDetachShader(program, fragmentShader);

        GL20.glDeleteShader(vertexShader);
        GL20.glDeleteShader(fragmentShader);

        if (GL20.glGetProgrami(program, GL20.GL_LINK_STATUS) != GL11.GL_TRUE) {
            String infoLog = GL20.glGetProgramInfoLog(program, GL20.glGetProgrami(program, GL20.GL_INFO_LOG_LENGTH));
            GL20.glDeleteProgram(program);
            System.err.println("Program linking failed: " + infoLog);

            return null;
        }

        return new Shader(program);
    }
}
