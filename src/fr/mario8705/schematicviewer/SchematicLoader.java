package fr.mario8705.schematicviewer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import com.flowpowered.nbt.ByteArrayTag;
import com.flowpowered.nbt.CompoundMap;
import com.flowpowered.nbt.CompoundTag;
import com.flowpowered.nbt.ShortTag;
import com.flowpowered.nbt.Tag;
import com.flowpowered.nbt.stream.NBTInputStream;

public final class SchematicLoader {
	public static Schematic loadSchematic(File f) throws IOException {
		try (InputStream in = new FileInputStream(f)) {
			return loadSchematic(in);
		}
	}

	public static Schematic loadSchematic(InputStream in) throws IOException {
		int width = 0, length = 0, height = 0;
		ByteArrayTag blocksTag = null;

		try (NBTInputStream nbtInput = new NBTInputStream(in)) {
			Tag<?> tag = nbtInput.readTag();

			if (!"Schematic".equals(tag.getName())) {
				throw new SchematicLoaderException("Invalid signature");
			}

			CompoundTag schematicTag = (CompoundTag) tag;
			CompoundMap schematicTagChilds = schematicTag.getValue();

			for (Tag<?> child : schematicTagChilds) {
				if ("Width".equals(child.getName())) {
					ShortTag widthTag = (ShortTag) child;

					width = widthTag.getValue().intValue();
				} else if ("Length".equals(child.getName())) {
					ShortTag lengthTag = (ShortTag) child;

					length = lengthTag.getValue().intValue();
				} else if ("Height".equals(child.getName())) {
					ShortTag heightTag = (ShortTag) child;

					height = heightTag.getValue().intValue();
				} else if ("Blocks".equals(child.getName())) {
					blocksTag = (ByteArrayTag) child;
				}
			}
		}

		if (width != 0 && height != 0 && length != 0 && blocksTag != null) {
			BlockState[][][] blockStates = new BlockState[width][height][length];

			for (int x = 0; x < width; x++) {
				for (int y = 0; y < height; y++) {
					for (int z = 0; z < length; z++) {
						blockStates[x][y][z] = new BlockState(
								Block.byID(blocksTag.getValue()[(y * length + z) * width + x] & 0xff));
					}
				}
			}

			return new Schematic(width, height, length, blockStates);
		}

		throw new SchematicLoaderException("Corrupt schematic file");
	}
}
