#version 330

uniform sampler2D uTexture;

varying vec3 vWorldPos;
varying vec2 vTexCoord;
varying vec3 vNormal;

layout (location = 0) out vec3 worldPosOut;
layout (location = 1) out vec4 diffuseOut;
layout (location = 2) out vec3 normalOut;
layout (location = 3) out vec3 texCoordOut;

void main() {
	vec4 diffuse = texture2D(uTexture, vTexCoord);

	if (diffuse.a <= 0.0)
		discard;
	
	worldPosOut = vWorldPos;
	diffuseOut = diffuse;
	normalOut = vNormal;
	texCoordOut = vec3(vTexCoord, 0.0);
}