#version 330

uniform sampler2D uPosition;
uniform sampler2D uAlbedo;
uniform sampler2D uNormal;
uniform sampler2D uTexCoord;

uniform vec3 uCameraPosition;
uniform vec2 uScreenSize;

struct Light {
    vec3 Position;
    vec3 Color;
    
    float Linear;
    float Quadratic;
};
const int NR_LIGHTS = 1;
 Light lights[NR_LIGHTS];

void main() {
    lights[0].Position = vec3(0.0f, 10.0f, 0.0f);
    lights[0].Color = vec3(1.0f);
    lights[0].Linear = 1.0f;
    lights[0].Quadratic = 1.0f;

	vec2 texCoord = gl_FragCoord.xy / uScreenSize;
	
	vec3 position = texture(uPosition, texCoord).rgb;
	vec3 albedo = texture(uAlbedo, texCoord).rgb;
	float specular = 0.1f;
	vec3 normal = texture(uNormal, texCoord).rgb * 2.0 - 1.0;
	
	vec3 viewDir = normalize(uCameraPosition - position);
	vec3 lightDir = normalize(vec3(0, 0, 0) - position);
	
	vec3 lighting = albedo;
	
	for(int i = 0; i < NR_LIGHTS; ++i)
    {
        // Diffuse
        vec3 lightDir = normalize(lights[i].Position - position);
        vec3 diffuse = max(dot(normal, lightDir), 0.0) * albedo * lights[i].Color;
        // Specular
        vec3 halfwayDir = normalize(lightDir + viewDir);  
        float spec = pow(max(dot(normal, halfwayDir), 0.0), 16.0);
        vec3 specular = lights[i].Color * spec * specular;
        // Attenuation
        float distance = length(lights[i].Position - position);
        float attenuation = 1.0 / (1.0 + lights[i].Linear * distance + lights[i].Quadratic * distance * distance);
        diffuse *= attenuation;
        specular *= attenuation;
        lighting += diffuse + specular;
    }
	
	gl_FragColor = vec4(albedo, 1.0);
}